﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainModel.Concrete
{
    public class MemberRepository
    {
        public int AddMember(string mName , string mFBID  )
        {
            NokatDBEntities entities = new NokatDBEntities();
            
            Member _Mem = new Member { Name = mName, FBID = mFBID };
            entities.Members.AddObject(_Mem);

            entities.SaveChanges();
            return _Mem.ID;

        }
    }
}
