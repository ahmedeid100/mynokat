﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainModel.Concrete
{
    public class JokeRepository
    {
        public int AddJoke(string mText, int mMemberID)
        {
            NokatDBEntities entities = new NokatDBEntities();

            Joke _Jok = new Joke { JokeText = mText, MemberID = mMemberID,TimeAdded=DateTime.Now.ToUniversalTime() , Rank = 0 };
            entities.Jokes.AddObject(_Jok);

            entities.SaveChanges();
            return _Jok.ID;

        }

        public int DecreaseRank(int mJokeID)
        {
            NokatDBEntities entities = new NokatDBEntities();

            var _rank = --(entities.Jokes.Where(p => p.ID == mJokeID).First().Rank);

            entities.SaveChanges();
            return _rank.Value;
        }

        public int IncreaseRank(int mJokeID)
        {
            NokatDBEntities entities = new NokatDBEntities();
            Joke _Jok = (Joke)(entities.Jokes.Where(p => p.ID == mJokeID));
            _Jok.Rank++;
            
            entities.SaveChanges();
            return _Jok.Rank.Value;
        }

        public List<Joke> GetInterestList()
        {
            NokatDBEntities entities = new NokatDBEntities();
            return entities.Jokes.OrderBy(p => p.TimeAdded.Value.Ticks * p.Rank).ToList();

        }
    
    }
}
